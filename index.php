<?php 
require "logica/Administrador.php";
    $pid = NULL;
    if (isset($_GET["pid"])) {
        $pid = base64_decode($_GET["pid"]);
    }
?>
<!Doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
    <link rel="icon" type="img/ico" href="img/imagenPortada.ico">
    <title>Amazonaz</title>
</head>
<body>
	<?php 
	   if (isset($pid)) {
	       include $pid;
	   }else {
	       include "presentacion/inicio.php";
	   }
	?>
</body>
</html>