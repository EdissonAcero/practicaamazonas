<?php 
class Conexion {
    private $MySQL;
    private $reseultado;
    
    function abrir() {
        $this -> MySQL = new mysqli("localhost", "root", "", "bdpracticaamazonas");
        $this -> MySQL -> set_charset("utf8");
    }
    
    function cerrar() {
        $this -> MySQL -> close();
    }
    
    function ejecutar($sentencia) {
        $this -> reseultado = $this -> MySQL -> query($sentencia);
    }
    
    function extraer() {
        return $this -> reseultado -> fetch_row();
    }
    
    function numFilas() {
        return ($this -> reseultado != NULL) ? $this -> reseultado -> num_rows : 0;
    }
}
?>